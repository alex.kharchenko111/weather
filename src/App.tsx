import React, { useState, useContext, useEffect } from "react";
import { Filter } from './components/Filter';
import { ForecastList } from './components/ForecastList';
import { useForecast } from './hooks';
import { ForecastModel } from './types';
import { ForecastCurrentDay } from './components/ForecastCurrentDay';

export const App: React.FC = () => {
    const { data: forecast } = useForecast();

    const [selectedItem, setSelectedItem] = useState<ForecastModel | null>(null);
    const onSelectedItem = (forecast: ForecastModel) => {
        setSelectedItem(forecast);
    };

    return (
            <main>
                <Filter/>
                <ForecastCurrentDay selectedItem={selectedItem}/>
                <div className="forecast">
                {forecast?.slice(0, 7).map(forecast =>
                    <ForecastList key={forecast.id}
                                  forecast={forecast}
                                  selectedItem={selectedItem}
                                  onSelectedItem={forecast => onSelectedItem(forecast)}/>
                )}
                </div>
            </main>
    );
};
