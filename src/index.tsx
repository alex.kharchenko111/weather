/* Core */
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

/* Components */
import { App } from './App';

/* Instruments */
import './theme/index.scss';
import { queryClient } from './lib/react-query';

render(
    <QueryClientProvider client={queryClient}>
            <App />
        <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>,
    document.getElementById('root'),
);
