import React from 'react';
import {  convertUnixTimeToDay } from '../helpers/formatter';
import { ForecastModel } from '../types';

export const ForecastList: React.FC<ForecastListProps> = ({forecast, onSelectedItem, selectedItem}) => {
    let forecastType;
    if (forecast.type === "sunny") {
        forecastType = "sunny";
    } else if (forecast.type === "rainy"){
        forecastType = "rainy";
    } else if (forecast.type === "cloudy"){
        forecastType = "cloudy";
    }

    return (
        <>
            <div className={`day ${forecastType}
                ${selectedItem?.id == forecast.id? 'selected' : null} `}
                 key={forecast.id}
                 onClick={() => onSelectedItem(forecast)}>
                <p>{convertUnixTimeToDay(forecast.day)}</p>
                <span>{forecast.temperature}</span>
            </div>
        </>
    )
}

/* Types */
interface ForecastListProps {
    forecast: ForecastModel;
    onSelectedItem:(forecast:ForecastModel) => void;
    selectedItem: ForecastModel | null;
}