/* Core */
import React, {FC} from "react";

/* Instruments */
import { ForecastModel } from '../types';
import { convertUnixTimeToDate, convertUnixTimeToDay } from '../helpers/formatter';

export const ForecastCurrentDay: FC<ForecastCurrentDayProps> = ({ selectedItem}) => {

    return (
        <>
            <div className="head">
                <div className="icon cloudy"/>
                <div className="current-date">
                    <p>{selectedItem ? convertUnixTimeToDay(selectedItem?.day) : null}</p>
                    <span>{selectedItem ? convertUnixTimeToDate(selectedItem?.day) : null}</span></div>
            </div>
            <div className={`current-weather`}>
                <p className="temperature">{selectedItem?.temperature}</p>
                <p className="meta">
                    <span className="rainy">%{selectedItem?.rain_probability}</span>
                    <span className="humidity">%{selectedItem?.humidity}</span>
                </p>
            </div>
        </>
    )
}

interface ForecastCurrentDayProps {
    selectedItem: ForecastModel | null;
}
