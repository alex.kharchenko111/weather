/* Core */
import waait from 'waait';
import { ForecastModel } from '../types';

/* Instruments */

export const api = {
    async getForecast() {
        const response = await fetch(`${process.env.REACT_APP_WEATHER_API_URL}`);
        const result: ForecastModel[] = (await response.json())?.data;
        await waait(1000);

        return result;
    }
};
