import moment from 'moment';

export function convertUnixTimeToDay(date: number) {
    return moment(date).format('dddd');
}

export function convertUnixTimeToDate(date: number) {
    return moment(date).format('LL');
}
