export interface ForecastModel {
    id: string;
    type: string;
    rain_probability: number;
    humidity: number;
    day: number;
    temperature: number;
}