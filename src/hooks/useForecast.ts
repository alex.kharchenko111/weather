/* Core */
import { useQuery } from 'react-query';

/* Instruments */
import { api } from '../api';
import { ForecastModel } from '../types';

export const useForecast = () => {
    const query = useQuery<ForecastModel[]>('forecast', api.getForecast);

    return query;
};
